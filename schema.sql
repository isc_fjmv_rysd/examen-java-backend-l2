-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para metaphorcebd
CREATE DATABASE IF NOT EXISTS `metaphorcebd` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `metaphorcebd`;

-- Volcando estructura para tabla metaphorcebd.contract
CREATE TABLE IF NOT EXISTS `contract` (
  `ContractId` bigint(20) NOT NULL AUTO_INCREMENT,
  `EmployeeId` int(11) NOT NULL,
  `ContractTypeId` smallint(6) NOT NULL,
  `DateFrom` datetime NOT NULL,
  `DateTo` datetime NOT NULL,
  `SalaryPerDay` decimal(20,6) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `DateCreated` datetime NOT NULL,
  PRIMARY KEY (`ContractId`),
  KEY `FK_contract_employee` (`EmployeeId`),
  KEY `FK_contract_contracttype` (`ContractTypeId`),
  CONSTRAINT `FK_contract_contracttype` FOREIGN KEY (`ContractTypeId`) REFERENCES `contracttype` (`ContractTypeId`),
  CONSTRAINT `FK_contract_employee` FOREIGN KEY (`EmployeeId`) REFERENCES `employee` (`EmployeeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla metaphorcebd.contracttype
CREATE TABLE IF NOT EXISTS `contracttype` (
  `ContractTypeId` smallint(6) NOT NULL DEFAULT '0',
  `Name` varchar(80) NOT NULL,
  `Description` varchar(255) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `DateCreated` datetime NOT NULL,
  PRIMARY KEY (`ContractTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

-- Volcando estructura para tabla metaphorcebd.employee
CREATE TABLE IF NOT EXISTS `employee` (
  `EmployeeId` int(11) NOT NULL AUTO_INCREMENT,
  `TaxIdNumber` varchar(13) NOT NULL,
  `Name` varchar(60) NOT NULL,
  `LastName` varchar(120) NOT NULL,
  `BirthDate` date NOT NULL,
  `Email` varchar(60) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `DateCreated` datetime NOT NULL,
  PRIMARY KEY (`EmployeeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
