package com.mscfc.examenjavabackend.repository;

import com.mscfc.examenjavabackend.model.ContractType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
public interface ContractTypeRepository extends JpaRepository<ContractType, Long> {
}
