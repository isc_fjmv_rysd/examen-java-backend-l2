package com.mscfc.examenjavabackend.repository;
import com.mscfc.examenjavabackend.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
