package com.mscfc.examenjavabackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamenjavabackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenjavabackendApplication.class, args);
	}

}
