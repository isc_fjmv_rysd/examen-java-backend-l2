package com.mscfc.examenjavabackend.model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long employeeId;

    @Column(name = "TaxIdNumber")
    private String taxIdNumber;

    @Column(name = "Name")
    private String name;

    @Column(name = "LastName")
    private String lastName;

    @Column(name = "BirthDate")
    private String birthDate;

    @Column(name = "Email")
    private String eMail;

    @Column(name = "IsActive")
    private String isActive;

    @Column(name = "DateCreated")
    private String dateCreated;
}
